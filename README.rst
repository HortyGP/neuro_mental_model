Mental Model
============
 |Mental_Model|

Criação de um modelo mental baseado em um conjunto de livros referência.

 |docs| |python| |license| |gitlab|


:Author:  Carlo E. T. Oliveira
:Version: 21.07
:Affiliation: Universidade Federal do Rio de Janeiro
:License: GNU General Public License v3 or later (GPLv3+)
:Homepage: `NeuroI 21 Mental Model`_
:Changelog: `CHANGELOG <CHANGELOG.rst>`_

Neuro I 21 Mental Model
------------------------

Proponho uma simples investigação para correlacionar a ontologia com a matriz,
Fazer uma contagem dos termos na ontologia nos pdfs dos livros disponíveis.
Como: converter pdf pata txt (no linux tem o programa pdftotext que faz isso).
Gerar o esquema do Gephi em uma forma textual legível (possivelmente JSON). 
Ler o grafo para uma estrutura interna (NetworkX no Python). 
Gerar também um dicionário com os termos ler os textos dos livros e contar as palavras no dicionário 
(se for preciso usar outro dicionário com termos equivalentes em inglês). 
Usar as contagens como peso no NetworkX. Mostrar o grafo resultante usando o plot do NetworkX. 
Ideia extra: inventar uma maneira de levantar pesos para arestas conectando duas palavras e plotar o resultado.

-------

Laboratório de Automação de Sistemas Educacionais
-------------------------------------------------

**Copyright © Carlo Olivera**

LABASE_ - NCE_ - UFRJ_

|LABASE|

.. _LABASE: http://labase.activufrj.nce.ufrj.br
.. _NCE: http://nce.ufrj.br
.. _UFRJ: http://www.ufrj.br

.. _NeuroI 21 Mental Model: https://activufrj.nce.ufrj.br/wiki/MODELOSCOGNITIVOS/Nero_I_21_Modelo_Mental

.. |gitlab| image:: https://img.shields.io/github/v/release/kwarwp/kwarwp
   :target: https://gitlab.com/labasence/neuro_mental_model


.. |LABASE| image:: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png
   :target: http://labase.activufrj.nce.ufrj.br
   :alt: LABASE

.. |Mental_Model| image:: https://i.imgur.com/HfrEgkv.png
   :target: https://activufrj.nce.ufrj.br/wiki/MODELOSCOGNITIVOS/Nero_I_21_Modelo_Mental
   :alt: MENTAL MODEL

.. |python| image:: https://img.shields.io/github/languages/top/kwarwp/kwarwp
   :target: https://www.python.org/downloads/release/python-383/

.. |docs| image:: https://img.shields.io/readthedocs/supygirls
   :target: https://i.imgur.com/hib4z1f.png

.. |license| image:: https://img.shields.io/github/license/kwarwp/kwarwp
   :target: https://gitlab.com/labasence/neuro_mental_model/-/blob/main/LICENSE
