Mental Model
============
Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_.


`Unreleased`_
-------------
- to be defined

`21.7`_
----------------

Added
+++++
- Document Structure.
- Some draft code.


-------

Laboratório de Automação de Sistemas Educacionais
-------------------------------------------------

**Copyright © Carlo Olivera**

LABASE_ - NCE_ - UFRJ_

|LABASE|

.. _LABASE: http://labase.activufrj.nce.ufrj.br
.. _NCE: http://nce.ufrj.br
.. _UFRJ: http://www.ufrj.br
.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _21.07: https://gitlab.com/labasence/neuro_mental_model/-/tags

.. |LABASE| image:: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png
   :target: http://labase.activufrj.nce.ufrj.br
   :alt: LABASE


